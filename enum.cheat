% enum, http

# python command-line HTTP client
httpie

# HTTPS using openssl
openssl s_client -connect example.com:443

# WAF fingerprinting
wafw00f example.com

% enum, netbios, smb

# list NetBios cache (from windows)
nbtstat -c

# list remote machine's NetBios (from windows)
nbtstat -a machine-name
nbtstat -A 1.2.3.4

# list SMB connections (from windows)
net use

# list SMB shared resources of specified target (from windows)
net view \\1.2.3.4

# connect to a shared resource (from windows)
net use \\1.2.3.4 "password" /user:"username"
net use \\1.2.3.4\folder "password" /user:"username"

# connect to a shared resource using null session (from windows)
net use \\1.2.3.4 "" /user:""

# map a shared resource to a device unit (from windows)
net use x: \\1.2.3.4\folder
net use x: \\1.2.3.4\folder /delete

# list remote machine's NetBios
nbtscan -r 1.2.3.0/24

# list SMB shared resource
smbclient -L //1.2.3.4 -N       # no password (null session)
smbclient -L //1.2.3.4 "senha" -U "username" -W "workgroup_or_domain"
smbclient -L //1.2.3.4 --option='client min protocol=NT1'  # force old smb version

# connect to a SMB shared resource
smbclient //1.2.3.4/folder "senha" -U "username"

# execute MS-RPC functions interactive mode
rpcclient -U username%password 1.2.3.4

# execute MS-RPC functions non-interactive mode
rpcclient -U username%password -c 'enumdomusers' 1.2.3.4

# wrapper to automate tools like: smbclient, rpclient, net and nmblookup
enum4linux.pl -a 1.2.3.4

# list SMB hosts
crackmapexec smb targets

% enum, nfs

# probe rpcbind on host and list of all registered RPC programs
rpcinfo -p 1.2.3.4

# show the NFS server's export list
showmount -e 1.2.3.4

# mount NFS export
mount -t nfs -o nfsvers=3 1.2.3.4:/opt /mnt

% enum, snmp

# SNMP scanner using a set of community strings from a file
onesixtyone -c dict.txt -i hosts.txt -w 100 -o output.txt

# enumerate the SNMP devices and output in a human friendly format
snmp-check -c public 1.2.3.4

# query a SNMP host for a tree of information using SNMP GETNEXT requests
snmpwalk -c public -v 2c 1.2.3.4

# communicates with a network entity using SNMP SET requests
snmpset -c public -v 2c 1.2.3.4 OID TYPE VALUE

# translate MIB OID names between numeric and textual forms
snmptranslate -On -IR sysDescr
snmptranslate -Onf -IR sysDescr
snmptranslate -Td -OS .1.3.6.1.2.1.1.1
